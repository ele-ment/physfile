﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace PhysFile.Exception_Classes
{
    [Serializable]
    public class DbDataReaderException : Exception
    {
        public DbDataReaderException()
            : base() { }

        public DbDataReaderException(string message)
            : base(message) { }

        public DbDataReaderException(string format, params object[] args)
            : base(string.Format(format, args)) { }

        public DbDataReaderException(string message, Exception innerException)
            : base(message, innerException) { }

        public DbDataReaderException(string format, Exception innerException, params object[] args)
            : base(string.Format(format, args), innerException) { }

        protected DbDataReaderException(SerializationInfo info, StreamingContext context)
            : base(info, context) { }
    }
}
