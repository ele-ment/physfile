﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace PhysFile.Exception_Classes
{
    [Serializable]
    public class DbConnectionException : System.Exception
    {
        public DbConnectionException()
            : base() { }

        public DbConnectionException(string message)
            : base(message) { }

        public DbConnectionException(string format, params object[] args)
            : base(string.Format(format, args)) { }

        public DbConnectionException(string message, Exception innerException)
            : base(message, innerException) { }

        public DbConnectionException(string format, Exception innerException, params object[] args)
            : base(string.Format(format, args), innerException) { }

        protected DbConnectionException(SerializationInfo info, StreamingContext context)
            : base(info, context) { }
    }
}
