﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;

namespace PhysFile
{
   public class dbConnection
    {
        public DbConnection CreateDbConnection(string providerName, string connectionString)
        {
            //string methodname = System.Reflection.MethodBase.GetCurrentMethod().Name;
            // Assume failure.
            DbConnection connection = null;

            // Create the DbProviderFactory and DbConnection.
            if (connectionString != null)
            {
                try
                {
                    DbProviderFactory factory =
                        DbProviderFactories.GetFactory(providerName);
                    
                    connection = factory.CreateConnection();
                    connection.ConnectionString = connectionString;
                }
                catch (Exception ex)
                {
                    // Set the connection to null if it was created.
                    if (connection != null)
                    {
                        connection = null;
                    }
                    throw ex;
                }
            }
            // Return the connection.
            return connection;
        }

        public static DbParameter CreateDbParameter(string providerName, System.Data.DbType paramType, string paramName, object paramValue)
        {
            DbProviderFactory factory = DbProviderFactories.GetFactory(providerName);
            DbParameter p = factory.CreateParameter();
            p.DbType = paramType;
            p.ParameterName = paramName;
            if (String.IsNullOrEmpty(paramValue.ToString()))
            {
                p.Value = DBNull.Value;
            }
            else
            {
                p.Value = paramValue;
            }

            return p;
        }


    }
}
