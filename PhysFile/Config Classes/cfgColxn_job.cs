﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace PhysFile
{
    public class cfgColxn_job : ConfigurationElementCollection
    {
        public jobElement this[int index]
        {
            get { return base.BaseGet(index) as jobElement; }
            set
            {
                if (BaseGet(index) != null)
                {
                    BaseRemoveAt(index);
                }
                BaseAdd(index, value);
            }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new jobElement();
        }

        protected override Object GetElementKey(ConfigurationElement element)
        {
            return ((jobElement)element).Name;
        }
    }
}
