﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Web;
using System.Xml;
using PhysFile.Config_Classes;

namespace PhysFile
{
    public class cfgSection_jobs : ConfigurationSection
    {
        [ConfigurationProperty("aJob", IsDefaultCollection = false)]
        [ConfigurationCollection(typeof(cfgColxn_job))]
        public cfgColxn_job jobCollection
        {
            get
            {
                return (cfgColxn_job)base["aJob"];
            }
        }
    }
}
