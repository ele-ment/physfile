﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace PhysFile
{
    public class DMLibraryElement : ConfigurationElement
    {

        public int ID { get; set; }
        public int DMVersion { get; set; } // these two properties are not configured, they are set in the background.

        [ConfigurationProperty("name")]
        public string Name
        {
            get
            {
                return this["name"] as string;
            }
        }

        [ConfigurationProperty("databaseType")]
        public string DatabaseType
        {
            get
            {
                return this["databaseType"] as string;
            }
        }

        [ConfigurationProperty("connectionString")]
        public string ConnectionString
        {
            get
            {
                return this["connectionString"] as string;
            }
        }



    } // end of class DMLibraryElement
}
