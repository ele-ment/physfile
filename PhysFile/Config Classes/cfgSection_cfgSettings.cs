﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Xml;
using PhysFile.Config_Classes;

namespace PhysFile
{
    public class cfgSection_cfgSettings : ConfigurationSection
    {
        [ConfigurationProperty("settings", IsDefaultCollection = false)]
        [ConfigurationCollection(typeof(cfgColxn_cfgSettings))]
        public cfgColxn_cfgSettings cfgSettings
        {
            get
            {
                return (cfgColxn_cfgSettings)base["settings"];
            }
        }
    }
}
