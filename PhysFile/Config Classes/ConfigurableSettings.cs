﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace PhysFile

{
    public class ConfigurableSettings : ConfigurationElement
    {
        [ConfigurationProperty("name")]
        public string Name
        {
            get
            {
                return this["name"] as string;
            }
        }

        [ConfigurationProperty("maxFailedAttempts")]
        public int? MaxFailedAttempts
        {
            get
            {
                return (int?)this["maxFailedAttempts"];
            }
        }

        [ConfigurationProperty("minutesUntilRetry")]
        public int? MinutesUntilRetry
        {
            get
            {
                return (int?)this["minutesUntilRetry"];
            }
        }

        [ConfigurationProperty("tickInterval")]
        public int TickInterval
        {
            get
            {
                return (int)this["tickInterval"];
            }
        }

        [ConfigurationProperty("pauseInterval")]
        public int PauseInterval
        {
            get
            {
                return (int)this["pauseInterval"];
            }
        }

        [ConfigurationProperty("batchSize")]
        public int BatchSize
        {
            get
            {
                return (int)this["batchSize"];
            }
        }

        [ConfigurationProperty("throttle")]
        public int Throttle
        {
            get
            {
                return (int)this["throttle"];
            }
        }

        [ConfigurationProperty("reportOnlyMode")]
        public bool ReportOnlyMode
        {
            get
            {
                return (this["reportOnlyMode"].ToString().ToUpper().Equals("TRUE")) ? true : false;
            }
        }

    } // end of class ConfigurableSettings
}
