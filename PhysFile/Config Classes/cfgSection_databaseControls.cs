﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Web;
using System.Xml;
using PhysFile.Config_Classes;

namespace PhysFile
{
    public class cfgSection_databaseControls : ConfigurationSection
    {
        [ConfigurationProperty("mydmLibrary", IsDefaultCollection = false)]
        [ConfigurationCollection(typeof(cfgColxn_dmle))]
        public cfgColxn_dmle dmLibraryCollection
        {
            get
            {
                return (cfgColxn_dmle)base["mydmLibrary"];
            }
        }
    }
}
