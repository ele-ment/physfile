﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace PhysFile.Config_Classes
{
    public class cfgColxn_cfgSettings : ConfigurationElementCollection
    {
        public ConfigurableSettings this[int index]
        {
            get { return base.BaseGet(index) as ConfigurableSettings; }
            set
            {
                if (BaseGet(index) != null)
                {
                    BaseRemoveAt(index);
                }
                BaseAdd(index, value);
            }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new ConfigurableSettings();
        }

        protected override Object GetElementKey(ConfigurationElement element)
        {
            return ((ConfigurableSettings)element).Name;
        }
    }
}


