﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace PhysFile
{
    public class cfgColxn_dmle : ConfigurationElementCollection
    {

        public DMLibraryElement this[int index]
        {
            get { return base.BaseGet(index) as DMLibraryElement; }
            set
            {
                if (BaseGet(index) != null)
                {
                    BaseRemoveAt(index);
                }
                BaseAdd(index, value);
            }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new DMLibraryElement();
        }

        protected override Object GetElementKey(ConfigurationElement element)
        {
            return ((DMLibraryElement)element).Name;
        }
    }
}
