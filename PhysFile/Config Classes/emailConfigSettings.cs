﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADVFPS.Core
{
    public class emailConfigSettings
    {
        public string emailMappings { get; set; }
        public int overWriteTo { get; set; }
        public int overWriteFrom { get; set; }
        public int overWriteCC { get; set; }
        public int overWriteBCC { get; set; }
        public bool setAttachIndicatorOnInline { get; set; }
        public int useOldEmailAddresses { get; set; }

        // column lengths and data types
        public int toColLength { get; set; }
        public string toDataType { get; set; }
        public int fromColLength { get; set; }
        public string fromDataType { get; set; }
        public int ccColLength { get; set; }
        public string ccDataType { get; set; }
        public int bccColLength { get; set; }
        public string bccDataType { get; set; }
        public int mailIDColLength { get; set; }
        public string mailIDDataType { get; set; } 
        public int parentMailIDColLength { get; set; }
        public string parentMailIDDatatype { get; set; }

        public emailConfigSettings(jobElement jbe)
        {
            emailMappings = jbe.EmailMapping;
            overWriteTo = jbe.OverwriteTO;
            overWriteFrom = jbe.OverwriteFROM;
            overWriteCC = jbe.OverwriteCC;
            overWriteBCC = jbe.OverwriteBCC;
            setAttachIndicatorOnInline = (jbe.SetAttachIndicatorOnInline.ToUpper().Equals("TRUE"));
            useOldEmailAddresses = jbe.UseOldEmailAddresses;
        }

        public void setColumnLengths(int to, int from, int cc, int bcc, int mail, int parentmail)
        {
            toColLength = to;
            fromColLength = from;
            ccColLength = cc;
            bccColLength = bcc;
            mailIDColLength = mail;
            parentMailIDColLength = parentmail;
        }

        public void setDataTypes(string to, string from, string cc, string bcc, string mail, string parentmail)
        {
            toDataType = to;
            fromDataType = from;
            ccDataType = cc;
            bccDataType = bcc;
            mailIDDataType = mail;
            parentMailIDDatatype = parentmail;
        }

        public bool isOKToWrite(string field, bool dmFieldIsBlank)
        {
            bool ret = false;
            switch (field)
            {
                case "TO":
                    ret = (overWriteTo == 3 || (overWriteTo == 2 && dmFieldIsBlank));
                    break;
                case "FROM":
                    ret = (overWriteFrom == 3 || (overWriteFrom == 2 && dmFieldIsBlank));
                    break;
                case "CC":
                    ret = (overWriteCC == 3 || (overWriteCC == 2 && dmFieldIsBlank));
                    break;
                case "BCC":
                    ret = (overWriteBCC == 3 || (overWriteBCC == 2 && dmFieldIsBlank));
                    break;

            }
            return ret;
        }

    }
}
