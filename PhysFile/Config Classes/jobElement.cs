﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace PhysFile
{
    public class jobElement : ConfigurationElement
    {
        public DateTime lastRunTime { get; set; }
        public bool timeToRunMeAgain { get; set; }

        [ConfigurationProperty("name")]
        public string Name
        {
            get
            {
                return this["name"] as string;
            }
        }

        [ConfigurationProperty("jobID")]
        public int JobID
        {
            get
            {
                return (int)this["jobID"];
            }
        }

        [ConfigurationProperty("jobType")]
        public string JobType
        {
            get
            {
                return this["jobType"] as string;
            }
        }

        [ConfigurationProperty("target")]
        public string Target
        {
            get
            {
                return this["target"] as string;
            }
        }

        [ConfigurationProperty("frequencyInSeconds")]
        public int FrequencyInSeconds
        {
            get
            {
                return (int)this["frequencyInSeconds"];
            }
        }
     
        [ConfigurationProperty("jobActivated")]
        public string Activated
        {
            get
            {
                return this["jobActivated"] as string;
            }
        }

        //[ConfigurationProperty("emailMapping")]
        //public string EmailMapping
        //{
        //    get
        //    {
        //        return this["emailMapping"] as string;
        //    }
        //}

        //[ConfigurationProperty("overwriteTO")]
        //public int OverwriteTO
        //{
        //    get
        //    {
        //        return (int)this["overwriteTO"];
        //    }
        //}

        //[ConfigurationProperty("overwriteFROM")]
        //public int OverwriteFROM
        //{
        //    get
        //    {
        //        return (int)this["overwriteFROM"];
        //    }
        //}

        //[ConfigurationProperty("overwriteCC")]
        //public int OverwriteCC
        //{
        //    get
        //    {
        //        return (int)this["overwriteCC"];
        //    }
        //}

        //[ConfigurationProperty("overwriteBCC")]
        //public int OverwriteBCC
        //{
        //    get
        //    {
        //        return (int)this["overwriteBCC"];
        //    }
        //}

        //[ConfigurationProperty("setAttachIndicatorOnInline")]
        //public string SetAttachIndicatorOnInline
        //{
        //    get
        //    {
        //        return this["setAttachIndicatorOnInline"] as string;
        //    }
        //}

        //[ConfigurationProperty("useOldEmailAddresses")]
        //public int UseOldEmailAddresses
        //{
        //    get
        //    {
        //        return (int)this["useOldEmailAddresses"];
        //    }
        //}
    } // end of class jobElement
}
