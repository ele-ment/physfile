﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;

using System.Threading.Tasks;
using System.Timers;

namespace PhysFile
{
    public partial class PhysFileService : ServiceBase
    {

        private const string sSource = "Element_PhysFile";
        private const string sLog = "Element_PhysFile log";
        private static bool tickedOnce = false;

        public PhysFileService()
        {
            if (!EventLog.SourceExists(sSource))
                EventLog.CreateEventSource(sSource, sLog);
            EventLog.WriteEntry(sSource, "InitializeComponent() has been called.");
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            EventLog.WriteEntry(sSource, "OnStart() has been called.");
            try
            {
                PFengine.OnStart();
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry(sSource, "OnStart() had exception: " + ex.ToString());
                EventLog.WriteEntry(sSource, ex.StackTrace);
                OnStop();


            }
        }

             protected override void OnStop()
        {
            EventLog.WriteEntry(sSource, "OnStop() has been called.");
            PFengine.OnStop();
        }

        protected void OnTimer(object sender, System.Timers.ElapsedEventArgs args)
        {
            if (!tickedOnce)
            {
                tickedOnce = true;
                EventLog.WriteEntry(sSource, "Timer has ticked.");
            }
            PFengine.OnTimer(sender, args);
        }



        internal void TestStartupAndStop(string[] args)
        {
            this.OnStart(args);
            Console.ReadLine();
            this.OnStop();
        }

    }


}





