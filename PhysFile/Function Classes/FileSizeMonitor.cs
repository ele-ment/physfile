﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Threading;
using System.IO;
//using PhysFile.Database_Classes;
using PhysFile.Config_Classes;
using Oracle.ManagedDataAccess.Client;
using System.Windows.Forms;

namespace PhysFile
{
    public class FileSizeMonitor
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private int maxAttempts, minutesUntilRetry, batchSize, throttle;
        private bool reportOnlyMode;
        private static dbConnection connPool; 
        private static DMLibraryElement dmle;
        private static string providerName;
        
        public FileSizeMonitor(ConfigurableSettings cfg, ref dbConnection c, DMLibraryElement d)
        {
            maxAttempts = cfg.MaxFailedAttempts ?? 5;
            minutesUntilRetry = cfg.MinutesUntilRetry ?? 10;
            batchSize = cfg.BatchSize;
            throttle = cfg.Throttle;
            reportOnlyMode = cfg.ReportOnlyMode;
            connPool = c;
            dmle = d;
            if (dmle.DatabaseType.ToLower().Equals("sql")) providerName = @"System.Data.SqlClient";
            else if (dmle.DatabaseType.ToLower().Equals("oracle")) providerName = @"System.Data.OracleClient"; //@"Oracle.DataAccess.Client"; 
            
        }

        public void execute()
        {
            logger.Trace("DM Version identified as: " + dmle.DMVersion);
            // open connection, retrieve the batch table
            logger.Trace("PysFile FileSizeMonitor Module started for library " + dmle.Name);
            if (maxAttempts < 1) maxAttempts = 1;
            if (batchSize < 1) batchSize = 100;
            if (minutesUntilRetry < 1) minutesUntilRetry = 1;

            DataTable qTable = new DataTable();            
            string docServerLoc = string.Empty, filePath = string.Empty, fileName = string.Empty;
            try // Top level catch-all
            {
                //if (isTimeToPreClean()) preCleanerOccasional();
                preCleanerRegular();
                qTable = getBatchTable();   //Retrn batch table of docs to process 
                // At this point we have the queue table
                bool justStarting = true;



                foreach (DataRow qTableRow in qTable.Rows)    //loop through each row in batch table 
                {
                    if (justStarting)
                    {
                        logger.Debug("Element PhysFile has begun processing documents.");
                        justStarting = false;
                    }


                    int processCode = (int)statusOfFiles.availableToProcess;   //process codes live in enum.cs

                    processCode = determineStatus(qTableRow);

                    if (processCode == (int)statusOfFiles.deferProcessing)
                    {
                        logger.Debug("Processing of docNumber " + qTableRow["docNumber"].ToString() + " is deferred due to status code " + qTableRow["status"].ToString());
                        // v1.3.4 - Added delay of items which are unavailable due to status for some reason.
                        DbConnection conn = connPool.CreateDbConnection(providerName, dmle.ConnectionString);
                        using (conn)
                        {
                            conn.Open();
                            queryToDelayProcessing(conn, "Deferred due to status " + qTableRow["status"].ToString(), qTableRow["number_of_attempts"].ToString(), Convert.ToInt32(qTableRow["docNumber"]), Convert.ToInt32(qTableRow["version_id"]));
                        }
                        continue;
                    }





                    int rowsAffected = processRecord(qTableRow, processCode, reportOnlyMode);
                    if (!reportOnlyMode && (rowsAffected == 0 || rowsAffected > 1))
                    {
                        logger.Error(rowsAffected + " rows were returned for docNumber " + qTableRow["docNumber"].ToString());



                    }




                }


                   


            } // end of top-level try          
            catch (Exception ex)
            {
                throw ex; // back to function core
            }
            finally
            {
                LogManager.Flush();
                logger.Trace("Processing of batch is complete.");
            }
            Thread.Sleep(throttle);
        } // end of execute()

        public DataTable getBatchTable()
        {
            logger.Trace("Creating batch for processing.");
            DataTable ret = new DataTable();
            DbConnection conn = connPool.CreateDbConnection(providerName, dmle.ConnectionString);
            string batchTableQueryText = string.Empty;

            // v1.3.4 - Changed order to status date ascending. This prioritizes the stuff that's
            // eligible but which hasn't been touched in the longest time. This is critical to 
            // avoid continuous reprocessing of items with problems (e.g. documents checked out that
            // get delayed over and over and over again.
            if (dmle.DatabaseType.ToLower().Equals("sql"))
            {
                batchTableQueryText = @"SELECT TOP " + batchSize + @" q.*, v.version, v.version_label, p.docserver_loc, p.application, p.storagetype, p.status, p.path ""ppath"", c.PATH ""cpath"", c.VERSION_ID ""componentsVID"", a.APPLICATION
                        FROM docsadm.version_updated_queue q 
                        left outer join docsadm.versions v ON v.version_id = q.version_id AND v.docnumber = q.docnumber
                        left outer join docsadm.profile p ON p.docnumber = q.docnumber
                        left outer join docsadm.components c ON c.docnumber = q.docnumber AND c.version_id = q.VERSION_ID
                        left outer join docsadm.APPS a ON p.APPLICATION = a.SYSTEM_ID
                        WHERE q.status_flag =0
                            AND (number_of_attempts IS NULL OR number_of_attempts < @maxAttempts) 
                            AND (q.delayedSince IS NULL OR DATEADD(MINUTE, @minUntilRetry, q.delayedSince) < GETDATE()) 
                            AND q.serviceName = 'file size monitor'
                            AND (DATEADD(SECOND, 30, q.status_date) < GETDATE())
                            ORDER BY status_date asc;";
            }
            else if (dmle.DatabaseType.ToLower().Equals("oracle"))
            {

                   batchTableQueryText = @"SELECT q.*, v.version, v.version_label, p.docserver_loc, p.application, p.storagetype, p.status, p.path as ppath, c.PATH as cpath, c.VERSION_ID as componentsVID, a.APPLICATION
                        FROM docsadm.version_updated_queue q 
                        left outer join docsadm.versions v ON v.version_id = q.version_id AND v.docnumber = q.docnumber
                        left outer join docsadm.profile p ON p.docnumber = q.docnumber
                        left outer join docsadm.components c ON c.docnumber = q.docnumber AND c.version_id = q.VERSION_ID
                        left outer join docsadm.APPS a ON p.APPLICATION = a.SYSTEM_ID
                        WHERE q.status_flag = 0 
                            AND (number_of_attempts IS NULL OR number_of_attempts < :maxAttempts) 
                            AND (q.delayedSince IS NULL OR ((q.delayedSince + INTERVAL '" + Convert.ToInt32(minutesUntilRetry).ToString() + @"' MINUTE) < SYSDATE)) 
                            AND q.serviceName = 'file size monitor'
                            AND ((q.status_date + INTERVAL '30' SECOND) < SYSDATE)
                            AND rownum <= :batchSize                        
                            ORDER BY status_date asc";
            }
            using (conn)
            {
                try
                {
                    conn.Open();
                    using (DbCommand batchTableQuery = conn.CreateCommand())
                    {
                        batchTableQuery.CommandText = batchTableQueryText;
                        batchTableQuery.CommandType = CommandType.Text;
                        logger.Debug("getBatchTable query : " + batchTableQueryText);                                                 
                        DbParameter w = dbConnection.CreateDbParameter(providerName, DbType.Int32, "maxAttempts", Convert.ToInt32(maxAttempts));                             batchTableQuery.Parameters.Add(w);
                        if (dmle.DatabaseType.ToLower().Equals("sql"))
                        {
                            batchTableQuery.Parameters.Add(dbConnection.CreateDbParameter(providerName, DbType.Int32, "minUntilRetry", Convert.ToInt32(minutesUntilRetry)));
                        }
                        else if (dmle.DatabaseType.ToLower().Equals("oracle"))
                        {
                            batchTableQuery.Parameters.Add(dbConnection.CreateDbParameter(providerName, DbType.Int32, "batchSize", Convert.ToInt32(batchSize)));
                        }

                        
                        using (DbDataReader reader = batchTableQuery.ExecuteReader()) { ret.Load(reader); }
                    } // end of using
                    
                    logger.Trace("Batch table built with " + ret.Rows.Count + " rows.");                                                 
                }
                catch
                {
                    throw;
                }
                finally
                {
                    conn.Close();
                }
            }         
            return ret;
        }

       // status codes returned are all integers, taken from the enum "statusOfFiles"
        public static int determineStatus(DataRow qTableRow)
        {
            try
            {
                int docNumber = Convert.ToInt32(qTableRow["docNumber"]);
                string vLabel = (qTableRow["version_label"] == DBNull.Value) ? "" : (string)qTableRow["version_label"];
                int statusColumn = -1;
                if (qTableRow["status"] == DBNull.Value)
                {
                    logger.Trace("Doc status is null " + DateTime.Now);
                    return (int)statusOfFiles.nullDocStatus;
                }
                else statusColumn = Convert.ToInt32(qTableRow["status"]);
                logger.Trace("Determining appropriate action for docnumber " + docNumber + ", version " + vLabel + ", with Profile.status code " + statusColumn);

                int vID = (qTableRow["componentsVID"] == DBNull.Value) ? -1 : Convert.ToInt32(qTableRow["componentsVID"]);

                if (vID == -1) return (int)statusOfFiles.missingComponentsRecord;
                else if (!isVIDUniqueInComponents(vID)) return (int)statusOfFiles.versionIDNonUniqueInComponents;
                if (vLabel.Length == 0) return (int)statusOfFiles.missingVersionsRecord;

                // deal with status column here
                if (statusColumn != 0 && statusColumn != 19)
                {
                    switch (statusColumn)
                    {
                        case 1: return (int)statusOfFiles.deferProcessing;  // doc being edited
                        case 2: return (int)statusOfFiles.deferProcessing;  // profile being edited
                        case 3: return (int)statusOfFiles.deferProcessing;  // checked out
                        case 4: return (int)statusOfFiles.deferProcessing;  // not available
                        case 5: return (int)statusOfFiles.deferProcessing;  // being indexed
                        case 6: return (int)statusOfFiles.archived;         // archived
                        case 16: return (int)statusOfFiles.archived;        // being archived
                        case 18: return (int)statusOfFiles.statusSetToDelete; // Deleted document
                        default: return (int)statusOfFiles.unsupportedStatusCode;
                    }
                }

                if (qTableRow["storagetype"].ToString().ToUpper().Equals("P")) return (int)statusOfFiles.paperProfile; // paper profile, ignore
                if (qTableRow["Application1"].ToString().ToUpper().Equals("FOLDER")) return (int)statusOfFiles.ignoreRow; //folder, ignore 


                // check to see if docServerLoc, filePath, and fileName are legit values
                string docServerLoc = "";
                if (qTableRow["docserver_loc"] == DBNull.Value)
                {
                    logger.Debug("null docserver_loc for docNumber " + docNumber + " and version " + vLabel);
                    return (int)statusOfFiles.docServerInaccessible;
                }
                else
                {
                    docServerLoc = qTableRow["docserver_loc"].ToString();
                    DirectoryInfo justDocServer = new DirectoryInfo(docServerLoc);
                    if (!justDocServer.Exists)
                    {
                        logger.Warn("Error trying to reach document server " + qTableRow["docserver_loc"].ToString() + " for docnumber " + docNumber + " and version " + vLabel);
                        return (int)statusOfFiles.docServerInaccessible;
                    }
                }
                // at this point docserver_loc can be considered legit

                string filePath = "";
                if (qTableRow["ppath"] == DBNull.Value)
                {
                    logger.Debug("null profile.path for docNumber " + docNumber + " and version " + vLabel);
                    return (int)statusOfFiles.filePathInaccessible; // set status flag, log a warning about file, skip row
                }
                else
                {
                    filePath = qTableRow["ppath"].ToString();
                    DirectoryInfo serverPlusPath = new DirectoryInfo(docServerLoc + filePath);
                    if (!serverPlusPath.Exists)
                    {
                        logger.Warn("Could not read file path: " + docServerLoc + filePath);
                        return (int)statusOfFiles.filePathInaccessible; // could be file system problem, don't try this again.
                    }
                }
                // at this point docServerLoc and filePath are considered legit

                if (qTableRow["cpath"] == DBNull.Value)
                {
                    logger.Warn("File name is missing for docNumber " + docNumber + " and version " + vLabel);
                    return (int)statusOfFiles.nullFileName; // this is a data failure, don't try again
                }

                FileInfo record = new FileInfo(docServerLoc + filePath + qTableRow["cpath"].ToString());
                if (!record.Exists) return (int)statusOfFiles.missingFile; // components references a missing file  


                if (record.Length == 0)
                {
                    logger.Warn("Zero Byte File " + docNumber + " and version " + vLabel);
                   
                }





                logger.Trace("ready to process docNumber " + docNumber);
            }
            catch (Exception ex)
            {
                logger.Error("error in determineStatus() " + ex.ToString());
                throw ex;
            }
            return (int)statusOfFiles.availableToProcess; // because all data are perfect little angels
        }

        public static int processRecord(DataRow qTableRow, int processCode, bool reportOnlyMode)
        {


            logger.Trace("Enter FIleSize Monitor processRecord Routine");
            int rowsAffected = 0;

            
            DbConnection conn = connPool.CreateDbConnection(providerName, dmle.ConnectionString);
            string docServerLoc = (string)qTableRow["docserver_loc"];
            string filePath = (qTableRow["ppath"] == DBNull.Value) ? null : (string)qTableRow["ppath"];
            string fileName = (qTableRow["cpath"] == DBNull.Value) ? null : (string)qTableRow["cpath"];
            string vLabel = (qTableRow["version_label"] == DBNull.Value) ? null : (string)qTableRow["version_label"];
            int docNumber = Convert.ToInt32(qTableRow["docNumber"]);
            int version_id = Convert.ToInt32(qTableRow["version_id"]);
            if (qTableRow["number_of_attempts"] == DBNull.Value) qTableRow["number_of_attempts"] = 0;
            string increment = (1 + Convert.ToInt32(qTableRow["number_of_attempts"])).ToString();
            logger.Trace("Beginning processing of docnumber " + docNumber + " with process code " + processCode + " in " + dmle.Name);
            try
            {                                          
                using (conn)
                {
                    conn.Open();                  
                    switch (processCode)
                    {
                        case (int)statusOfFiles.availableToProcess: // process this file                           
                            string[] pathStrings = PFengine.sanitizeFilePath(docServerLoc, filePath, fileName);
                            FileInfo record = new FileInfo(pathStrings[0] + pathStrings[1] + pathStrings[2]);
                            long fileSize = 0;
                            fileSize = record.Length; // the real meat of FPS File Size Monitor is this one line.   
                            

                            //add code to 

                            //if (fileSize > Int32.MaxValue) // a rare case
                            //{
                            //    if (!reportOnlyMode)
                            //    {
                            //        using (DbCommand writeTooLargeFileSizeUpdate = conn.CreateCommand())
                            //        {
                            //            writeTooLargeFileSizeUpdate.CommandType = CommandType.Text;
                            //            if (dmle.DatabaseType.ToLower().Equals("sql")) writeTooLargeFileSizeUpdate.CommandText = sql_statements.writeTooLargeFileSizeUpdate;
                            //            else if (dmle.DatabaseType.ToLower().Equals("oracle")) writeTooLargeFileSizeUpdate.CommandText = Oracle_statements.writeTooLargeFileSizeUpdate;                                    
                            //            writeTooLargeFileSizeUpdate.Parameters.Add(dbConnection.CreateDbParameter(providerName, DbType.Int64, "sz", fileSize));
                            //            writeTooLargeFileSizeUpdate.Parameters.Add(dbConnection.CreateDbParameter(providerName, DbType.Int32, "dn", docNumber));
                            //            writeTooLargeFileSizeUpdate.Parameters.Add(dbConnection.CreateDbParameter(providerName, DbType.Int32, "vz", version_id));                                     
                            //            writeTooLargeFileSizeUpdate.ExecuteNonQuery();
                            //        }
                            //        using (DbCommand writeTooLargeFileSizeInsert = conn.CreateCommand())
                            //        {
                            //            writeTooLargeFileSizeInsert.CommandType = CommandType.Text;
                            //            if (dmle.DatabaseType.ToLower().Equals("sql"))
                            //            {
                            //                writeTooLargeFileSizeInsert.CommandText = sql_statements.writeTooLargeFileSizeInsert;
                            //                writeTooLargeFileSizeInsert.Parameters.Add(dbConnection.CreateDbParameter(providerName, DbType.Int32, "dn", docNumber));
                            //                writeTooLargeFileSizeInsert.Parameters.Add(dbConnection.CreateDbParameter(providerName, DbType.Int32, "vz", version_id));
                            //                writeTooLargeFileSizeInsert.Parameters.Add(dbConnection.CreateDbParameter(providerName, DbType.Int32, "dn2", docNumber));
                            //                writeTooLargeFileSizeInsert.Parameters.Add(dbConnection.CreateDbParameter(providerName, DbType.Int32, "vz2", version_id));
                            //                writeTooLargeFileSizeInsert.Parameters.Add(dbConnection.CreateDbParameter(providerName, DbType.Int64, "sz", fileSize));
                            //            }
                            //            else if (dmle.DatabaseType.ToLower().Equals("oracle"))
                            //            {
                            //                writeTooLargeFileSizeInsert.CommandText = Oracle_statements.writeTooLargeFileSizeInsert;                                        
                            //                writeTooLargeFileSizeInsert.Parameters.Add(dbConnection.CreateDbParameter(providerName, DbType.Int32, "dn", docNumber));
                            //                writeTooLargeFileSizeInsert.Parameters.Add(dbConnection.CreateDbParameter(providerName, DbType.Int32, "vz", version_id));
                            //                writeTooLargeFileSizeInsert.Parameters.Add(dbConnection.CreateDbParameter(providerName, DbType.Int64, "sz", fileSize));
                            //                writeTooLargeFileSizeInsert.Parameters.Add(dbConnection.CreateDbParameter(providerName, DbType.Int32, "dn", docNumber));
                            //                writeTooLargeFileSizeInsert.Parameters.Add(dbConnection.CreateDbParameter(providerName, DbType.Int32, "vz", version_id));
                            //            }
                            //            rowsAffected = writeTooLargeFileSizeInsert.ExecuteNonQuery();
                            //        }                                                                   
                            //        using (DbCommand writeMinusThreeToComponents = conn.CreateCommand())
                            //        {
                            //            if (dmle.DatabaseType.ToLower().Equals("sql")) writeMinusThreeToComponents.CommandText = sql_statements.writeMinusThreeForTooLargeFile;
                            //            else if (dmle.DatabaseType.ToLower().Equals("oracle")) writeMinusThreeToComponents.CommandText = Oracle_statements.writeMinusThreeForTooLargeFile;
                            //            writeMinusThreeToComponents.CommandType = CommandType.Text;
                            //            writeMinusThreeToComponents.Parameters.Add(dbConnection.CreateDbParameter(providerName, DbType.Int32, "dn", docNumber));
                            //            writeMinusThreeToComponents.Parameters.Add(dbConnection.CreateDbParameter(providerName, DbType.Int32, "vz", version_id));
                            //            rowsAffected = writeMinusThreeToComponents.ExecuteNonQuery();
                            //        }                                                                                                    
                            //        logger.Debug("File size of " + fileSize + " written to COMPONENTS_ADV_FILE_SIZES for docnumber " + docNumber + ", version " + vLabel);
                            //    }
                            //    else logger.Debug("Proposed: File size of " + fileSize + " written to COMPONENTS_ADV_FILE_SIZES for docnumber " + docNumber + ", version " + vLabel);
                            //}
                            //else
                            //{
                                if (!reportOnlyMode)
                                {
                                    using (DbCommand writebackFileSize = conn.CreateCommand())
                                    {
                                        //if (dmle.DatabaseType.ToLower().Equals("sql")) writebackFileSize.CommandText = sql_statements.writebackFileSize;
                                        //else if (dmle.DatabaseType.ToLower().Equals("oracle")) writebackFileSize.CommandText = Oracle_statements.writebackFileSize;
                                        //writebackFileSize.CommandType = CommandType.Text;
                                        //writebackFileSize.Parameters.Add(dbConnection.CreateDbParameter(providerName, DbType.Int32, "sz", fileSize));
                                        //writebackFileSize.Parameters.Add(dbConnection.CreateDbParameter(providerName, DbType.Int32, "dn", docNumber));
                                        //writebackFileSize.Parameters.Add(dbConnection.CreateDbParameter(providerName, DbType.Int32, "vz", version_id));
                                        //rowsAffected = writebackFileSize.ExecuteNonQuery();
                                        rowsAffected = 1;
                                    }
                                                                     
                                    if (fileSize == 0) logger.Warn("Document at: " + docServerLoc + filePath + fileName + " with docNumber " + docNumber + ", version " + vLabel + " is 0 bytes.");
                                    else logger.Debug("Document at: " + docServerLoc + filePath + fileName + " with docNumber " + docNumber + ", version " + vLabel + " is " + fileSize + " bytes.");
                                }
                                else
                                {
                                    if (fileSize == 0) logger.Warn("Proposed: Document at: " + docServerLoc + filePath + fileName + " with docNumber " + docNumber + ", version " + vLabel + " is 0 bytes.");
                                    else logger.Debug("Proposed: Document at: " + docServerLoc + filePath + fileName + " with docNumber " + docNumber + ", version " + vLabel + " is " + fileSize + " bytes.");
                                }                            
                           // }
                            
                            if (rowsAffected == 1 || reportOnlyMode) // happy path
                            {
                                if (!reportOnlyMode)
                                {
                                    using (DbCommand success = conn.CreateCommand())
                                    {
                                        if (dmle.DatabaseType.ToLower().Equals("sql")) success.CommandText = sql_statements.successUpdateForFileSizeMonitor;
                                        else if (dmle.DatabaseType.ToLower().Equals("oracle")) success.CommandText = Oracle_statements.successUpdateForFileSizeMonitor;
                                        success.CommandType = CommandType.Text;
                                        success.Parameters.Add(dbConnection.CreateDbParameter(providerName, DbType.Int32, "sFlag", 1));
                                        success.Parameters.Add(dbConnection.CreateDbParameter(providerName, DbType.String, "attempts", increment));
                                        success.Parameters.Add(dbConnection.CreateDbParameter(providerName, DbType.String, "msg", "File Sucessfully Reviewed"));
                                        success.Parameters.Add(dbConnection.CreateDbParameter(providerName, DbType.Int32, "dn", docNumber));
                                        success.Parameters.Add(dbConnection.CreateDbParameter(providerName, DbType.Int32, "vz", version_id));
                                        rowsAffected = success.ExecuteNonQuery();
                                    }
                                    logger.Debug("Record " + docNumber + ", version " + vLabel + " in " + dmle.Name + " scanned.");
                                }
                                else logger.Debug("Proposed: Record " + docNumber + ", version " + vLabel + " in " + dmle.Name + " scanned.");
                            }
                            else // set record to delay and try again, increment attempts
                            {
                                if (!reportOnlyMode)
                                {
                                    string message = "Expected successful check to 1 row, returned " + rowsAffected + " rows.";
                                    queryToDelayProcessing(conn, message, increment, docNumber, version_id);                                                                
                                    logger.Debug("Processing delayed for record " + docNumber + ", version " + vLabel + " in " + dmle.Name);          
                                }
                                else logger.Debug("Proposed: Processing delayed for record " + docNumber + ", version " + vLabel + " in " + dmle.Name);                                               
                            }
                            break;
                        case (int)statusOfFiles.docServerInaccessible: // set delay so record will be tried again later
                            if (!reportOnlyMode)
                            {
                                string message = "Processing delayed: Document server is null or is otherwise inaccessible.";
                                queryToDelayProcessing(conn, message, increment, docNumber, version_id);                                                              
                                logger.Debug("Processing delayed for document number " + docNumber + " in " + dmle.Name);            
                            }
                            else logger.Debug("Proposed: Processing delayed for document number " + docNumber + " in " + dmle.Name);
                            break;
                        case (int)statusOfFiles.nullDocStatus: // set delay so record will be tried again later
                            if (!reportOnlyMode)
                            {
                                string message = "Processing delayed: Document status is null or is otherwise inaccessible.";
                                queryToDelayProcessing(conn, message, increment, docNumber, version_id);                              
                                logger.Debug("Processing delayed for document number " + docNumber + " in " + dmle.Name);
                            }
                            else logger.Debug("Proposed: Processing delayed for document number " + docNumber + " in " + dmle.Name);
                            break;
                        case (int)statusOfFiles.paperProfile: // Ignore because it's a paper profile                       
                            if (!reportOnlyMode)
                            {
                                using (DbCommand setRecordToIgnore = conn.CreateCommand())
                                {
                                    if (dmle.DatabaseType.ToLower().Equals("sql")) setRecordToIgnore.CommandText = sql_statements.setRecordToIgnore;
                                    else if (dmle.DatabaseType.ToLower().Equals("oracle")) setRecordToIgnore.CommandText = Oracle_statements.setRecordToIgnore;
                                    setRecordToIgnore.CommandType = CommandType.Text;                                   
                                    setRecordToIgnore.Parameters.Add(dbConnection.CreateDbParameter(providerName, DbType.String, "attempts", increment));
                                    setRecordToIgnore.Parameters.Add(dbConnection.CreateDbParameter(providerName, DbType.String, "msg", "Paper profile"));
                                    setRecordToIgnore.Parameters.Add(dbConnection.CreateDbParameter(providerName, DbType.Int32, "dn", docNumber));
                                    setRecordToIgnore.Parameters.Add(dbConnection.CreateDbParameter(providerName, DbType.Int32, "vz", version_id));
                                    setRecordToIgnore.ExecuteNonQuery();    
                                }
                                logger.Info("Record " + docNumber + ", version " + vLabel + " in " + dmle.Name);
                            }
                            else logger.Info("Proposed: Record " + docNumber + ", version " + vLabel + " in " + dmle.Name);
                            break;
                        case (int)statusOfFiles.filePathInaccessible:
                            if (!reportOnlyMode)
                            {
                                queryToPermanentlyRejectRow(conn, increment, "File path is missing or is otherwise inaccessible", docNumber, version_id);                            
                                logger.Warn("Profile.path is null for docnumber " + docNumber + ", version " + vLabel);                        
                            }
                            else logger.Warn("Proposed: Profile.path is null for docnumber " + docNumber + ", version " + vLabel);
                            break;
                        case (int)statusOfFiles.nullFileName: // Permanent failure because of null components.path (file name)
                            if (!reportOnlyMode)
                            {                            
                                queryToPermanentlyRejectRow(conn, increment, "Components.path is null or is otherwise inaccessible", docNumber, version_id);
                                logger.Warn("Components.path is null for docnumber " + docNumber + ", version " + vLabel);                         
                            }
                            else logger.Warn("Proposed: Components.path is null for docnumber " + docNumber + ", version " + vLabel);                       
                            break;
                        case (int)statusOfFiles.missingFile: // We have a missing file that cannot be found.  Writing -1 to file size
                            //


                            if (!reportOnlyMode)
                            {                            
                                queryToPermanentlyRejectRow(conn, increment, "References a nonexistent file at that location.", docNumber, version_id);
                                using (DbCommand writebackFileSize = conn.CreateCommand())
                                {
                                    if (dmle.DatabaseType.ToLower().Equals("sql")) writebackFileSize.CommandText = sql_statements.writebackFileSize;
                                    else if (dmle.DatabaseType.ToLower().Equals("oracle")) writebackFileSize.CommandText = Oracle_statements.writebackFileSize;
                                    writebackFileSize.CommandType = CommandType.Text;
                                    writebackFileSize.Parameters.Add(dbConnection.CreateDbParameter(providerName, DbType.String, "sz", -1));
                                    writebackFileSize.Parameters.Add(dbConnection.CreateDbParameter(providerName, DbType.Int32, "dn", docNumber));
                                    writebackFileSize.Parameters.Add(dbConnection.CreateDbParameter(providerName, DbType.Int32, "vz", version_id));
                                    rowsAffected = writebackFileSize.ExecuteNonQuery();
                                }
                                logger.Warn("Record " + docNumber + ", version " + vLabel + " in " + dmle.Name + " references a missing file.");
                            }
                            logger.Warn("Proposed: Record " + docNumber + ", version " + vLabel + " in " + dmle.Name + " references a missing file.");
                            break;
                        case (int)statusOfFiles.missingComponentsRecord:
                            if (!reportOnlyMode)
                            {                           
                                queryToPermanentlyRejectRow(conn, increment, "Components record is missing", docNumber, version_id);
                                logger.Warn("Components record is absent for docnumber " + docNumber + ", version " + vLabel); 
                            }
                            else logger.Warn("Proposed: Components record is absent for docnumber " + docNumber + ", version " + vLabel);                                    
                            break;
                        case (int)statusOfFiles.versionIDNonUniqueInComponents:
                            if (!reportOnlyMode)
                            {                              
                                queryToPermanentlyRejectRow(conn, increment, "Version_ID is not unique in Components", docNumber, version_id);
                                logger.Warn("Version_ID " + version_id + " is not unique in Components table.");         
                            }
                            else logger.Warn("Proposed: Version_ID " + version_id + " is not unique in Components table.");                          
                            break;
                        case (int)statusOfFiles.missingVersionsRecord:
                            if (!reportOnlyMode)
                            {                              
                                queryToPermanentlyRejectRow(conn, increment, "Component has no corresponding record in Versions.", docNumber, version_id);
                                logger.Warn("Component with version_ID " + version_id + " has no corresponding record in Versions");
                            }
                            else logger.Warn("Proposed: Component with version_ID " + version_id + " has no corresponding record in Versions");
                            break;
                        case (int)statusOfFiles.statusSetToDelete:
                            if (!reportOnlyMode)
                            {                               
                                queryToPermanentlyRejectRow(conn, increment, "DM indicates document was deleted.", docNumber, version_id);
                                logger.Debug("Document number " + docNumber + ", version " + vLabel + " references a deleted version.");
                            }
                            else logger.Debug("Proposed: Document number " + docNumber + ", version " + vLabel + " references a deleted version.");
                            break;
                        case (int)statusOfFiles.archived:
                            if (!reportOnlyMode)
                            {                            
                                queryToPermanentlyRejectRow(conn, increment, "Document version is archived.", docNumber, version_id);
                                logger.Debug("Document number " + docNumber + ", version " + vLabel + " references an archived version.");
                            }
                            else logger.Debug("Proposed: Document number " + docNumber + ", version " + vLabel + " references an archived version.");                     
                            break;
                        case (int)statusOfFiles.unsupportedStatusCode:
                            if (!reportOnlyMode)
                            {                               
                                queryToPermanentlyRejectRow(conn, increment, "Unsupported document status code: " + qTableRow["status"].ToString(), docNumber, version_id);   
                                logger.Debug("Document number " + docNumber + ", version " + vLabel + " has an unsupported status code requiring manual intervention.");
                            }
                            else logger.Debug("Proposed: Document number " + docNumber + ", version " + vLabel + " has an unsupported status code requiring manual intervention.");
                            break;
                    } // end of status code switch
                } // end of using(conn)             
            } // end of try
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                logger.Trace("Finished processing docnumber " + docNumber + " with process code " + processCode + " in " + dmle.Name);


                conn.Close();
            }


            //DataRow dr = badTable.NewRow();
            //dr = qTableRow;
            //badTable.ImportRow(dr);


            return rowsAffected;
        }


        
        public static void queryToDelayProcessing(DbConnection conn, string message, string attempts, int docnum, int vzID)
        {
            using (DbCommand delay = conn.CreateCommand())
            {
                if (dmle.DatabaseType.ToLower().Equals("sql")) delay.CommandText = sql_statements.setDelayForFileSizeMonitor;
                else if (dmle.DatabaseType.ToLower().Equals("oracle")) delay.CommandText = Oracle_statements.setDelayForFileSizeMonitor;
                delay.CommandType = CommandType.Text;              
                delay.Parameters.Add(dbConnection.CreateDbParameter(providerName, DbType.String, "msg", message));
                delay.Parameters.Add(dbConnection.CreateDbParameter(providerName, DbType.String, "attempts", attempts));
                delay.Parameters.Add(dbConnection.CreateDbParameter(providerName, DbType.Int32, "dn", docnum));
                delay.Parameters.Add(dbConnection.CreateDbParameter(providerName, DbType.Int32, "vz", vzID));
                delay.ExecuteNonQuery();
            }          
        }

        public static void queryToPermanentlyRejectRow(DbConnection conn, string attempts, string message, int docNumber, int version_ID)
        {
            using (DbCommand rejectRow = conn.CreateCommand())
            {
                if (dmle.DatabaseType.ToLower().Equals("sql")) rejectRow.CommandText = sql_statements.PermanentlyRejectRow;
                else if (dmle.DatabaseType.ToLower().Equals("oracle")) rejectRow.CommandText = Oracle_statements.PermanentlyRejectRow;
                rejectRow.CommandType = CommandType.Text;
                rejectRow.Parameters.Add(dbConnection.CreateDbParameter(providerName, DbType.String, "attempts", attempts));
                rejectRow.Parameters.Add(dbConnection.CreateDbParameter(providerName, DbType.String, "msg", message));
                rejectRow.Parameters.Add(dbConnection.CreateDbParameter(providerName, DbType.Int32, "dn", docNumber));
                rejectRow.Parameters.Add(dbConnection.CreateDbParameter(providerName, DbType.Int32, "vz", version_ID));
                rejectRow.ExecuteNonQuery();
            }
        }
       
        public void preCleanerOccasional()
        {
            logger.Trace("Beginning occasional SQL pre-cleaning procedure");
            DbConnection conn = connPool.CreateDbConnection(providerName, dmle.ConnectionString);
            using (conn)
            {
                try
                {
                    conn.Open();
                    using (DbCommand pcComp = conn.CreateCommand())
                    {
                        if (dmle.DatabaseType.ToLower().Equals("sql")) pcComp.CommandText = sql_statements.preCleanComponents;
                        else if (dmle.DatabaseType.ToLower().Equals("oracle")) pcComp.CommandText = Oracle_statements.preCleanComponents;
                        pcComp.CommandType = CommandType.Text;
                        pcComp.ExecuteNonQuery();
                    }

                    using (DbCommand pcVers = conn.CreateCommand())
                    {
                        if (dmle.DatabaseType.ToLower().Equals("sql")) pcVers.CommandText = sql_statements.preCleanVersionsFSM;
                        else if (dmle.DatabaseType.ToLower().Equals("oracle")) pcVers.CommandText = Oracle_statements.preCleanVersionsFSM;
                        pcVers.CommandType = CommandType.Text;
                        pcVers.ExecuteNonQuery();
                    }
                }
                catch
                {
                    throw;
                }
                finally
                {
                    conn.Close();
                }
            } // end of using(conn)

            logger.Trace("Occasional SQL pre-cleaning procedure complete.");
        }

        public void preCleanerRegular()
        {
            logger.Trace("Beginning SQL pre-cleaning procedure");
            DbConnection conn = connPool.CreateDbConnection(providerName, dmle.ConnectionString);
            using (conn)
            {
                try
                {
                    conn.Open();
                    string commandText = "";

                    for (int i = 1; i <= 4; i++)
                    {
                        if (dmle.DatabaseType.ToLower().Equals("sql"))     // original order in old FPS service  1. DataInteg 2. CleanNonUnique 3. precleanFolders 
                        {
                            switch (i)
                            {
                                case 1: commandText = sql_statements.preCleanExcludeFolders; break;
                                case 2: commandText = sql_statements.preCleanDataIntegrity1; break;
                                case 3: commandText = sql_statements.preCleanNonUniqueVIDVersions; break;
                                case 4: commandText = sql_statements.deleteProcessedFilesAndFolders; break;
                            }  
                        }
                        else if (dmle.DatabaseType.ToLower().Equals("oracle")) 
                        {
                            switch (i)
                            {
                                case 1: commandText = Oracle_statements.preCleanExcludeFolders; break;
                                case 2: commandText = Oracle_statements.preCleanDataIntegrity1; break;
                                case 3: commandText = Oracle_statements.preCleanNonUniqueVIDVersions; break;
                                case 4: commandText = Oracle_statements.deleteProcessedFilesandFolders; break;
                            }  
                        } 
                                           
                        using (DbCommand pcX = conn.CreateCommand())
                        {
                            pcX.CommandText = commandText;
                            pcX.CommandType = CommandType.Text;
                            pcX.ExecuteNonQuery(); 
                        }
                        switch (i)
                        {
                            case 1: logger.Debug("Exclude Folders check #1 executed."); break;
                            case 2: logger.Debug("Data integrity check #2 executed."); break;
                            case 3: logger.Debug("Non Unique data integrity check #3 executed."); break;
                            case 4: logger.Debug("Removed Folders and sucessfully processed files from the version_updated_queue ."); break;
                        }
                    }
                }
                catch
                {
                    throw;
                }
                finally
                {
                    conn.Close();
                }
            } // end of using(conn)
            logger.Trace("SQL pre-cleaning procedure complete.");
        }

        private bool isTimeToPreClean()
        {
            bool ret = false;
            TimeSpan start = TimeSpan.Parse("03:00"); // 3 AM
            TimeSpan end = TimeSpan.Parse("03:05");   // 3:05 AM
            TimeSpan now = DateTime.Now.TimeOfDay;

            if (start <= end)
            {
                // start and stop times are in the same day
                if (now >= start && now <= end)
                {
                    // current time is between start and stop
                    ret = true;
                }
            }
            else
            {
                // start and stop times are in different days
                if (now >= start || now <= end)
                {
                    // current time is between start and stop
                    ret = true;
                }
            }
            return ret;
        }

        private static bool isVIDUniqueInComponents(int versionID)
        {
            DbConnection conn = connPool.CreateDbConnection(providerName, dmle.ConnectionString);
            int count = 0;
            using (conn)
            {
                conn.Open();
                using (DbCommand x = conn.CreateCommand())
                {
                    x.CommandType = CommandType.Text;
                    if (dmle.DatabaseType.ToLower().Equals("sql")) x.CommandText = sql_statements.isVIDUniqueInComponents;
                    else if (dmle.DatabaseType.ToLower().Equals("oracle")) x.CommandText = Oracle_statements.isVIDUniqueInComponents;
                    x.Parameters.Add(dbConnection.CreateDbParameter(providerName, DbType.Int32, "vz", versionID));
                    count = Convert.ToInt32(x.ExecuteScalar());
                }              
            }
            if (count < 1) throw new Exception("Version ID not found in Components table.");
            else if (count > 1) return false;
            return true;
        }

    } // end of class FileSizeMonitor


    public static class DataTableExtensions
    {
        public static void WriteToCsvFile(this DataTable dataTable, string filePath)
        {
            StringBuilder fileContent = new StringBuilder();

            foreach (var col in dataTable.Columns)
            {
                fileContent.Append(col.ToString() + ",");
            }

            fileContent.Replace(",", System.Environment.NewLine, fileContent.Length - 1, 1);



            foreach (DataRow dr in dataTable.Rows)
            {

                foreach (var column in dr.ItemArray)
                {
                    fileContent.Append("\"" + column.ToString() + "\",");
                }

                fileContent.Replace(",", System.Environment.NewLine, fileContent.Length - 1, 1);
            }

            System.IO.File.WriteAllText(filePath, fileContent.ToString());

        }








        public static string ToJSON(this DataTable table)
        {
            var JSONString = new StringBuilder();
            if (table.Rows.Count > 0)
            {
                JSONString.Append("[");
                for (int i = 0; i < table.Rows.Count; i++)
                {
                    JSONString.Append("{");
                    for (int j = 0; j < table.Columns.Count; j++)
                    {
                        if (j < table.Columns.Count - 1)
                        {
                            JSONString.Append("\"" + table.Columns[j].ColumnName.ToString() + "\":" + "\"" + table.Rows[i][j].ToString() + "\",");
                        }
                        else if (j == table.Columns.Count - 1)
                        {
                            JSONString.Append("\"" + table.Columns[j].ColumnName.ToString() + "\":" + "\"" + table.Rows[i][j].ToString() + "\"");
                        }
                    }
                    if (i == table.Rows.Count - 1)
                    {
                        JSONString.Append("}");
                    }
                    else
                    {
                        JSONString.Append("},");
                    }
                }
                JSONString.Append("]");
            }




            return JSONString.ToString();

        }


    }

}