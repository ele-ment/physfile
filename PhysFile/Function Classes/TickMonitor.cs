﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhysFile
{
    public sealed class TickMonitor
    {
        public int tickIdle
        {
            get
            {
                return int.MinValue;
            }
        }
        public int tickRunning { get; set; }
        public int tickNumber { get; set; }

        public void initialize()
        {
            this.tickRunning = this.tickIdle;
            this.tickNumber = this.tickIdle + 1;
        }

        public void nextTick()
        {
            this.tickNumber++;
        }

        public void iAmRunningNow()
        {
            this.tickRunning = this.tickNumber;
        }

        public void iAmDoneRunning()
        {
            this.tickRunning = this.tickIdle;
        }

        public bool isAnotherTickRunning()
        {
            return ((this.tickRunning != this.tickNumber) && (this.tickRunning != this.tickIdle));
        }
    } // end of class TickMonitor
}
