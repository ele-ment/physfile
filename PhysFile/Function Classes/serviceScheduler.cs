﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using PhysFile.Config_Classes;

namespace PhysFile
{
    public class serviceScheduler
    {
        public List<jobElement> jobSet;
        public bool ssInitialized { get; set; }
        public int howManyJobsToDo { get; set; }

        public void initialize()
        {
            try
            {
                ssInitialized = false;
                howManyJobsToDo = 0;
                cfgSection_jobs cfgSect = ConfigurationManager.GetSection("jobs") as cfgSection_jobs;
                int jobCount = cfgSect.jobCollection.Count;
                jobElement myJob = new jobElement();
                jobSet = new List<jobElement>();
                for (int i = 0; i < jobCount; i++)
                {
                    myJob = cfgSect.jobCollection[i];
                    jobSet.Add(myJob);
                }
                ssInitialized = true;
            }
            catch (Exception ex)
            {
                throw new Exception("Scheduler initialization failed: " + ex.Message);
            }

        }

        public void determineWhatNeedsToRun()
        {
            TimeSpan jobInterval;
            int countJobs = 0;
            foreach (jobElement job in jobSet)
            {
                if (!job.Activated.Equals("true")) continue;
                if (job.FrequencyInSeconds < 1) throw new ArithmeticException();   // prevent division errors               
                DateTime lastRun = job.lastRunTime;
                jobInterval = new TimeSpan(0, 0, 0, job.FrequencyInSeconds);              
                DateTime runDeadline = lastRun.Add(jobInterval);
                int pastDeadline = DateTime.Compare(runDeadline, DateTime.Now);
                if (pastDeadline < 0)
                {
                    job.timeToRunMeAgain = true;
                    countJobs++;
                }
            }
            this.howManyJobsToDo = countJobs;
        }

        public void markJobAsDone(int ID)
        {
            foreach (jobElement job in jobSet)
            {
                if (job.JobID == ID)
                {
                    job.timeToRunMeAgain = false;
                    job.lastRunTime = DateTime.Now;
                    howManyJobsToDo--;
                }
            }
        }
    } // end of class serviceScheduler
}
