﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using NLog;
using System.Threading;
using System.Threading.Tasks;
using PhysFile.Config_Classes;
//using PhysFile.Database_Classes;
using System.Configuration;
using PhysFile.Exception_Classes;
using System.Data.Common;
using System.Reflection;
//using PhysFile.Function_Classes;

namespace PhysFile
{
     public class PFengine
    {

        private static List<DMLibraryElement> dmLibrarySet;
        private static System.Timers.Timer masterTimer;
        private static bool systemFailure, initialized, isInitializing, enginePause, okToWork;
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private static serviceScheduler scheduler;
        private static TickMonitor tickMonitor;
        private static TimeSpan pauseInterval;
        private static ConfigurableSettings cfgSettings;
        private static dbConnection connPool;

        public static void OnStart()
        {
            try
            {
                InitializeService();
                int timerInterval = cfgSettings.TickInterval;
                if (timerInterval < 0) timerInterval = 10000;
                masterTimer = new System.Timers.Timer();
                masterTimer.Interval = timerInterval;
                masterTimer.AutoReset = true;
                masterTimer.Elapsed += new System.Timers.ElapsedEventHandler(OnTimer);
                masterTimer.Start();
            }
            catch (Exception ex)
            {
                logger.Error(ex.StackTrace);
            }
        }

        public static void OnStop()
        {
            systemRenewal(false); // kill everything without starting over
        }

        public static void systemRenewal(bool restart)
        {
            if (restart)
            {
                logger.Fatal("System failure encountered, restarting.");
            }
            else logger.Fatal("Shutting down");

            initialized = false;
            systemFailure = false;
        }

        public static void pauseService(TimeSpan pInterval)
        {
            logger.Info("Paused service at: " + DateTime.Now);
            Thread.Sleep(pInterval);
            enginePause = false;
        }

        public static void InitializeService()
        {
            try
            {
                logger.Trace("Element PysFile PFengine initialization beginning");
                isInitializing = true;
                cfgSection_cfgSettings cfgSect = ConfigurationManager.GetSection("cfg_able_Settings") as cfgSection_cfgSettings;
                cfgSettings = cfgSect.cfgSettings[0];
                if (cfgSettings.PauseInterval > 0) pauseInterval = new TimeSpan(0, 0, 0, 0, cfgSettings.PauseInterval);
                else pauseInterval = new TimeSpan(0, 1, 0);
                dmLibrarySet = new List<DMLibraryElement>();
                connPool = new dbConnection();
                buildLibSet();
                scheduler = new serviceScheduler();
                tickMonitor = new TickMonitor();
                scheduler.initialize();
                tickMonitor.initialize();
                initialized = true;
                systemFailure = false;
                logger.Trace("Service initialization complete.");
            }
            catch (Exception ex)
            {
                logger.Fatal("Initializer failed: " + ex.ToString());
                systemFailure = true;
            }
            finally
            {
                isInitializing = false;
            }
        } // end of InitializeTestService1()

        public static void OnTimer(object sender, System.Timers.ElapsedEventArgs args)
        {
            tickMonitor.nextTick();
            if (tickMonitor.isAnotherTickRunning() || isInitializing || enginePause) return;
            if (systemFailure || !initialized)
            {
                string message = string.Empty;
                if (systemFailure) message = "System failure detected, restarting";
                if (!initialized) message = "Uninitialized state encountered. restarting";
                errorResponse(new Exception(""), 3, message, true); // a generic response that drops us into the errorResponse Flow
                return;
            }
            try
            {
                tickMonitor.iAmRunningNow();
                okToWork = true;
                scheduler.determineWhatNeedsToRun();
                logger.Debug("scheduler has " + scheduler.howManyJobsToDo + " jobs in queue");
                if (scheduler.howManyJobsToDo == 0)
                {
                    logger.Debug("Found no jobs in current schedule.  Exiting.");
                    return;
                }

                foreach (jobElement job in scheduler.jobSet)
                {
                    if (okToWork && job.timeToRunMeAgain && (job.Activated.ToLower().Equals("true") || job.Activated.ToLower().Equals("y")))
                    {
                        DMLibraryElement targetDMLE = new DMLibraryElement();
                        foreach (DMLibraryElement dmle in dmLibrarySet)
                        {
                            if (job.Target.Equals(dmle.Name)) targetDMLE = dmle;
                        }
                        logger.Trace("Settings for library " + targetDMLE.Name + ": Database type: " + targetDMLE.DatabaseType);
                        logger.Trace("Starting job: " + job.Name);
                        if (job.JobType.Equals("fileSizeMonitor"))
                        {

                            FileSizeMonitor fsm = new FileSizeMonitor(cfgSettings, ref connPool, targetDMLE);
                            fsm.execute();
                        }


                            // FROM ORIGINAL FILE PROPERTY SERVICE CODE - NOT A FEATURE FOR PHYSfILE


                        //else if (job.JobType.Equals("emailMetadataExtractor"))
                        //{
                        //    emailConfigSettings ecs = new emailConfigSettings(job);
                        //    EmailMetaDataExtractor emde = new EmailMetaDataExtractor(cfgSettings, ref connPool, targetDMLE, ecs);
                        //    emde.execute();
                        //}
                         //else if (job.JobType.Equals("cleanQTable")) cleanQTable(targetDMLE);
                        // at this point, okToWork will be false if any of the job methods caused an error that triggered a restart


                        if (okToWork)
                        {
                            scheduler.markJobAsDone(job.JobID);
                            logger.Debug(job.Name + " executed successfully.");
                        }
                    }
                }
            }
            catch (DbConnectionException dbCex) // database connection fails to open
            {
                errorResponse(dbCex, 2, "Database connection issue: " + dbCex.GetType().ToString() + ": " + dbCex.ToString(), true); // triggers restart
            }
            catch (DbDataReaderException dbDrEx) // problem with DbDataReader
            {
                errorResponse(dbDrEx, 2, "Data Reader error: " + dbDrEx.GetType().ToString() + ": " + dbDrEx.ToString(), true); // triggers restart
            }
            catch (Exception ex) // Top level catch-all
            {
                errorResponse(ex, 2, "unexpected error of type " + ex.GetType().ToString() + ": " + ex.ToString() + ex.StackTrace, true); // for general unhandled errors--triggers restart
            }
            finally
            {
                tickMonitor.iAmDoneRunning();
            }
        } // end of onTimer()

        public static void buildLibSet()
        {
            logger.Trace("buildLibSet starting");
            try
            {
                cfgSection_databaseControls cfgSect = ConfigurationManager.GetSection("databaseControls") as cfgSection_databaseControls;
                int dbCount = cfgSect.dmLibraryCollection.Count;

                // v1.3.3 - BTO - Corrected this when multiple libraries are present by instantiating new object within loop and also only adding those with a name to avoid errors on blank entries.
                for (int i = 0; i < dbCount; i++)
                {
                    DMLibraryElement dmle = new DMLibraryElement();
                    if (cfgSect.dmLibraryCollection[i].Name.Trim() != "")
                    {
                        dmle = cfgSect.dmLibraryCollection[i];
                        dmle.ID = i;
                        dmle.DMVersion = setDMVersion(dmle);
                        dmLibrarySet.Add(dmle);
                        logger.Trace("Settings for library " + dmle.Name + ": Database type: " + dmle.DatabaseType);
                    }
                    else
                    { logger.Trace("Skipping library definition with no name in configuration file."); }
                }
                logger.Trace("Library set built");
            }
            catch (Exception ex)
            {
                logger.Error("Error in building set of DM libraries: " + ex.Message);
            }
            finally
            {
                logger.Trace("buildLibSet completed");
            }

        }

        private static int setDMVersion(DMLibraryElement dmle)
        {
            logger.Trace("setDMVersion starting");
            string providerName = "";
            int ret = 0;
            if (dmle.DatabaseType.ToLower().Equals("sql")) providerName = @"System.Data.SqlClient";
            else if (dmle.DatabaseType.ToLower().Equals("oracle")) providerName = @"System.Data.OracleClient";
            //else if (dmle.DatabaseType.ToLower().Equals("oracle")) providerName = @"Oracle.DataAccess.Client"; 
            string dmv = "";
            logger.Debug("setDMVersion Opening connection with provider " + providerName);
            DbConnection conn = connPool.CreateDbConnection(providerName, dmle.ConnectionString);
            try
            {
                using (conn)
                {
                    conn.Open();
                    using (DbCommand dmVersion = conn.CreateCommand())
                    {
                        dmVersion.CommandText = "SELECT VERSION FROM DOCSADM.DOCSPARMS"; // should work for both SQL and Oracle?
                        dmVersion.CommandType = CommandType.Text;
                        dmv = (string)dmVersion.ExecuteScalar();
                    }

                }
                if (dmv.Length == 0) return 0;
                int x = int.Parse(dmv.Substring(0, 1));
                if (x == 5) ret = 5;
                else ret = int.Parse(dmv.Substring(0, 2));

            }
            catch
            {
                logger.Error("setDMVersion : Error with trying to determine DM version.");
                throw;
            }
            finally
            {
                conn.Dispose();
                logger.Trace("setDMVersion completed " +"VERSION FOUND: "+ ret);
            }
            return ret;
        }

        private static int getMajorDMVersion(string v)
        {
            int ret = 0;
            if (v.Length == 0) return 0;
            int x = int.Parse(v.Substring(0, 1));
            if (x == 5) ret = 5;
            else ret = int.Parse(v.Substring(0, 2));
            return ret;
        }



        /**
         * level 0: Error
         * level 1: Warn
         * level 2: Fatal
         * */
        public static void errorResponse(Exception ex, int level, string message, bool restartCondition)
        {
            switch (level)
            {
                case 0:
                    logger.Error(message);
                    logger.Error(ex.ToString());
                    break;
                case 1:
                    logger.Warn(message);
                    logger.Warn(ex.ToString());
                    break;
                case 2:
                    logger.Fatal(message);
                    logger.Fatal(ex.ToString());
                    break;
            }
            if (restartCondition)
            {
                systemFailure = true;
                enginePause = true;
                okToWork = false;
                masterTimer.Stop();
                masterTimer.Close();
                masterTimer.Dispose();
                systemRenewal(true);
                pauseService(pauseInterval);
                OnStart();
            }
        }

        public static string[] sanitizeFilePath(string docServerLoc, string filePath, string fileName)
        {
            string[] pathStrings = new string[3];
            if (docServerLoc.Length > 0 && filePath.Length > 0 && fileName.Length > 0) // don't attempt to sanitize nonexistent files 
            {
                try
                {
                    if (docServerLoc.Substring(docServerLoc.Length - 1).Equals(@"\")) docServerLoc = docServerLoc.Substring(0, docServerLoc.Length - 1);
                    if (!filePath.Substring(0, 1).Equals(@"\")) filePath = @"\" + filePath;
                    if (!filePath.Substring(filePath.Length - 1).Equals(@"\")) filePath += (@"\");
                    filePath.Replace(@"\\", @"\");
                    if (fileName.Contains(@"\")) fileName.Replace(@"\", @"");

                    pathStrings[0] = docServerLoc;
                    pathStrings[1] = filePath;
                    pathStrings[2] = fileName;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return pathStrings;
        }
    }
}
