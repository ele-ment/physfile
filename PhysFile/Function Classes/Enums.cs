﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhysFile
{
    public enum emailCondition
    {
        notYetAssigned,
        emailOK,
        errorWithOpeningEmail,
        missingMail_ID,
        missingParentMail_ID,
        missingBothMailIDAndParentMailID,
        zeroByteEmail
    }

    public enum statusOfAttachments
    {
        noAttachments,
        onlyInline,
        atLeastOneTypical
    }

    
    public enum statusOfFiles
    {
        availableToProcess,
        successfullyProcessed,
        ignoreRow,
        permanentFailure,
        paperProfile,
        docServerInaccessible,
        filePathInaccessible,
        nullFileName,
        nullDocStatus,
        missingFile,
        missingComponentsRecord,
        versionIDNonUniqueInComponents,
        missingVersionsRecord,
        statusSetToDelete,
        deferProcessing,
        archived,
        unsupportedStatusCode,
        folder,
        zeroByteFile
    }
    
}
