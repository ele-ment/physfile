Configuration instructions for PhysFile for MnDOT

1. Download the latest code from Bitbucket (https://bitbucket.org/ele-ment/).
2. Open the PhysFile solution.
3. Edit the App.Config and customize the following line:

<add name="legal" databaseType="sql" connectionString="Server=hgdmserver;Database=hglegal1;User Id=sa;Password=*******"/>

4. Right click on the project and choose Properties. On the Application tab, change the output to "Console Application".
5. Build the project.
6. Next you need to setup the table and trigger that PhysFile depends on. Run the following statements against your SQL or Oracle instance:

On SQL Server Database *******************************************************************************************

CREATE TABLE "DOCSADM"."VERSION_UPDATED_QUEUE" 
   (           "DOCNUMBER" DECIMAL(38,0), 
                "VERSION_ID" DECIMAL(38,0), 
                "SERVICENAME" NVARCHAR(100), 
                "STATUS_FLAG" DECIMAL(38,0), 
                "NUMBER_OF_ATTEMPTS" DECIMAL(38,0), 
                "DELAYEDSINCE" DATETIME, 
                "STATUS_DATE" DATETIME, 
                "STATUS_MESSAGE" NVARCHAR(255)
   ) ;

CREATE INDEX STATUS_DATE_INDEX ON DOCSADM.VERSION_UPDATED_QUEUE (STATUS_DATE) 

CREATE INDEX QTABLEIDX ON DOCSADM.VERSION_UPDATED_QUEUE (VERSION_ID, DOCNUMBER) 

CREATE UNIQUE INDEX PREVENTDUPLICATEINSERTS ON DOCSADM.VERSION_UPDATED_QUEUE (DOCNUMBER, VERSION_ID, SERVICENAME) 


CREATE OR ALTER TRIGGER [DOCSADM].[ADV_QUEUE_ON_VERSION_CHANGE] 
ON [DOCSADM].[VERSIONS]
AFTER INSERT,UPDATE
AS
	BEGIN
		/* 04-29-2021 Added exclusion for FOLDER (STATUS_FLAG<>2) */
		
 		UPDATE docsadm.version_updated_queue 
                SET status_flag = 0, status_message = null, status_date = GETDATE(), delayedsince = null, number_of_attempts = 0
                WHERE DOCNUMBER = (select DOCNUMBER from inserted) AND VERSION_ID = (select VERSION_ID from inserted) AND STATUS_FLAG<>2;
				
		INSERT INTO docsadm.version_updated_queue(docnumber, version_id, servicename, status_flag, delayedSince, status_date)
		select i.DOCNUMBER, i.VERSION_ID, 'file size monitor', 0, null, GETDATE() from inserted i
		join DOCSADM.PROFILE p on i.DOCNUMBER = p.DOCNUMBER and p.APPLICATION<>2
		left outer join docsadm.version_updated_queue q2 on q2.docnumber = i.docnumber and q2.version_id = i.version_id and q2.servicename = 'file size monitor'
		where q2.version_id is null
		
  	END

On Oracle Database *******************************************************************************************

CREATE TABLE "DOCSADM"."VERSION_UPDATED_QUEUE" 
   (           "DOCNUMBER" NUMBER(*,0), 
                "VERSION_ID" NUMBER(*,0), 
                "SERVICENAME" NVARCHAR2(100), 
                "STATUS_FLAG" NUMBER(*,0), 
                "NUMBER_OF_ATTEMPTS" NUMBER(*,0), 
                "DELAYEDSINCE" DATE, 
                "STATUS_DATE" DATE, 
                "STATUS_MESSAGE" NVARCHAR2(255)
   ) ;

CREATE INDEX "DOCSADM"."STATUS_DATE_INDEX" ON "DOCSADM"."VERSION_UPDATED_QUEUE" ("STATUS_DATE") ;

CREATE INDEX "DOCSADM"."QTABLEIDX" ON "DOCSADM"."VERSION_UPDATED_QUEUE" ("VERSION_ID", "DOCNUMBER") ;

CREATE UNIQUE INDEX "DOCSADM"."PREVENTDUPLICATEINSERTS" ON "DOCSADM"."VERSION_UPDATED_QUEUE" ("DOCNUMBER", "VERSION_ID", "SERVICENAME") ;

CREATE OR REPLACE PACKAGE ADV_FPS_PKG
IS
   TRG_QUEUE_ON_VER_CHG_ENABLED   BOOLEAN := TRUE;
END;

TRIGGER ADV_QUEUE_ON_VERSION_CHANGE 
AFTER INSERT OR UPDATE
  ON DOCSADM.VERSIONS
  FOR EACH ROW
BEGIN
  IF NOT ADV_FPS_PKG.TRG_QUEUE_ON_VER_CHG_ENABLED THEN
    RETURN;
  END IF;

/* 04-29-2021 Added exclusion for FOLDER (STATUS_FLAG<>2) */
UPDATE docsadm.version_updated_queue
  SET status_flag = 0, status_message = null, status_date = SYSDATE, delayedsince = null, number_of_attempts = 0
  WHERE DOCNUMBER = :NEW.DOCNUMBER AND VERSION_ID = :NEW.VERSION_ID AND STATUS_FLAG<>2;

/* 04-09-2021 Added exclusion for FOLDER (APPLICATION<>2) */
  INSERT INTO docsadm.version_updated_queue(docnumber, version_id, servicename, status_flag, delayedSince, status_date)
  select :NEW.DOCNUMBER, :NEW.VERSION_ID, 'file size monitor', 0, null, SYSDATE
  from dual
  join DOCSADM.PROFILE p on :NEW.DOCNUMBER = p.DOCNUMBER and p.APPLICATION<>2
  left outer join docsadm.version_updated_queue q on q.docnumber = :NEW.DOCNUMBER and q.version_id = :NEW.VERSION_ID and q.servicename = 'file size monitor'
  where q.version_id is null ; -- no existing row.

/* MnDOT not Using EMAIL METADATA EXTRACTORr
  INSERT INTO docsadm.version_updated_queue(docnumber, version_id, servicename, status_flag, delayedSince, status_date)
  select :NEW.DOCNUMBER, :NEW.VERSION_ID, 'email metadata extractor', 0, null, SYSDATE
  from dual
  left outer join docsadm.version_updated_queue q on q.docnumber = :new.docnumber and q.version_id = :new.version_id and q.servicename = 'email metadata extractor'
  inner join docsadm.profile p on p.DOCNUMBER = :new.docnumber
    inner join docsadm.APPS a on p.APPLICATION = a.SYSTEM_ID
  where a.APPLICATION = 'MS OUTLOOK' AND q.version_id is null; */
END;

7. Run the project.
8. Open DM and create a new document.
9. Go to your database and query the version_updated_queue table. You should see a row for the 1st version of the document you just created.

FYI - Below is the query (formatted for SQL Server) that we use to find the physical file:

*********************************************************************************************************************

DECLARE @maxAttempts Integer = 5 ;
DECLARE  @minUntilRetry Integer = 10;

SELECT TOP 500 q.*, v.version, v.version_label, p.docserver_loc, p.application, p.storagetype, p.status,
p.path "ppath", c.PATH "cpath", c.VERSION_ID "componentsVID" /*, a.APPLICATION */
FROM docsadm.version_updated_queue q  
left outer join docsadm.versions v ON v.version_id = q.version_id AND v.docnumber = q.docnumber
left outer join docsadm.profile p ON p.docnumber = q.docnumber
left outer join docsadm.components c ON c.docnumber = q.docnumber AND c.version_id = q.VERSION_ID
--left outer join docsadm.APPS a ON p.APPLICATION = a.SYSTEM_ID
WHERE q.status_flag =0 AND (number_of_attempts IS NULL OR number_of_attempts < @maxAttempts)  
AND (q.delayedSince IS NULL OR DATEADD(MINUTE, @minUntilRetry, q.delayedSince) < GETDATE()) 
AND q.serviceName = 'file size monitor' AND (DATEADD(SECOND, 30, q.status_date) < GETDATE()) ORDER BY status_date asc;

*********************************************************************************************************************
